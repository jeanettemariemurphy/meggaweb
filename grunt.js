module.exports = function(grunt) {
 
	grunt.initConfig({
		
		concat: {
			meggaweb: {
				src: ['js/meggaweb/meggaweb.js', 'js/meggaweb/meggaweb.utils.js', 'js/meggaweb/meggaweb.constants.js', 'js/meggaweb/meggaweb.bootstrap.js'],
				dest: 'js/meggaweb-bundle.js' 
			},
			libs: {
				src: ['js/libs/jquery-1.9.0.min.js'],
				dest: 'js/libs-bundle.js'
			},
			ui: {
				src: ['js/ui/accordion.js', 'js/ui/hijaxLinks.js'],
				dest: 'js/ui-bundle.js'
			},
		}, 
		less: {
			main: {
				options: {
					yuicompress: false
				},
				files: {
					'css/styles.css': 'less/styles.less',
					'css/print-styles.css': 'less/print-styles.less'
				}
			}
		},

		watch: {
			files: [
				//'js/libs/*.js',
				//'less/*.less',
			//	'_scripts/macmillan.min.js',
			//	'_scripts/libs.min.js',
		//		'_scripts/ui.min.js',
				//'grunt.js'
			],
			tasks: 'less concat min'
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				immed: true,
				latedef: true,
				newcap: true,
				noarg: true,
				sub: true,
				undef: true,
				boss: true,
				eqnull: true
			},
			globals: {
				exports: true,
				module: false
			}
		},

		uglify: {}
	}); 

	grunt.loadNpmTasks('grunt-contrib-less');
	
	// Default task.
	//grunt.registerTask('default', 'less concat min');
	grunt.registerTask('default', 'less concat');
};
