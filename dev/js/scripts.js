
var loginForm = function (theForm) {
    var frm, inputContainer, inputs, goBtn;
    
    function init() {
        frm = theForm;
        inputContainer = frm.getElementsByTagName('fieldset')[0];
        inputs = inputContainer.getElementsByTagName('input');
        goBtn = getElementsByClassName('submit')[0];
        bindEvents();
    }
    
    function bindEvents() {
       addEvent('click', goBtn, checkValid);
    }
    
    function checkValid(e) {
        getElementsByClassName('confirmation')[0].style.display = '';
        if(getElementsByClassName('lt-ie9').length === 0){
            e.preventDefault();
        } else {
            e = window.event;
            e.returnValue = false;
        }
         for (var i = 0; i< inputs.length; i++) {
            var errorContainer = inputs[i].parentNode;
            // remove the error class name if it is there
            errorContainer.className =  errorContainer.className.replace(/\b error\b/,'');
           
            if (inputs[i].value.length === 0) {
               errorContainer.className += ' error';
            }
        }
       if(getElementsByClassName('error').length === 0) {
            // commented out so I can show example confirmation message without ajax and preventing page refresh
           // frm.submit();
            getElementsByClassName('confirmation')[0].style.display = 'block';
        }
        
    }
    
  
  return {
    init: init
  };

};

function addEvent(evnt, elem, func) {
   if (elem.addEventListener)  // W3C DOM
      elem.addEventListener(evnt,func,false);
   else if (elem.attachEvent) { // IE DOM
      elem.attachEvent("on"+evnt, func);
   }
   else { // No much to do
      elem[evnt] = func;
   }
}


/*
	Developed by Robert Nyman, http://www.robertnyman.com
	Code/licensing: http://code.google.com/p/getelementsbyclassname/
*/	
var getElementsByClassName = function (className, tag, elm){
	if (document.getElementsByClassName) {
		getElementsByClassName = function (className, tag, elm) {
			elm = elm || document;
			var elements = elm.getElementsByClassName(className),
				nodeName = (tag)? new RegExp("\\b" + tag + "\\b", "i") : null,
				returnElements = [],
				current;
			for(var i=0, il=elements.length; i<il; i+=1){
				current = elements[i];
				if(!nodeName || nodeName.test(current.nodeName)) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	else if (document.evaluate) {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = "",
				xhtmlNamespace = "http://www.w3.org/1999/xhtml",
				namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace : null,
				returnElements = [],
				elements,
				node;
			for(var j=0, jl=classes.length; j<jl; j+=1){
				classesToCheck += "[contains(concat(' ', @class, ' '), ' " + classes[j] + " ')]";
			}
			try	{
				elements = document.evaluate(".//" + tag + classesToCheck, elm, namespaceResolver, 0, null);
			}
			catch (e) {
				elements = document.evaluate(".//" + tag + classesToCheck, elm, null, 0, null);
			}
			while ((node = elements.iterateNext())) {
				returnElements.push(node);
			}
			return returnElements;
		};
	}
	else {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = [],
				elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
				current,
				returnElements = [],
				match;
			for(var k=0, kl=classes.length; k<kl; k+=1){
				classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
			}
			for(var l=0, ll=elements.length; l<ll; l+=1){
				current = elements[l];
				match = false;
				for(var m=0, ml=classesToCheck.length; m<ml; m+=1){
					match = classesToCheck[m].test(current.className);
					if (!match) {
						break;
					}
				}
				if (match) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	return getElementsByClassName(className, tag, elm);
};
