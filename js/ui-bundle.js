
var accordion = function () {
  var $wrapper = $('div.accordion'),
      $itemWrapper = $('article', $wrapper),
      $trigger = $('h2 a', $itemWrapper),
      $itemBody = $('div.content', $itemWrapper),
      aniSpeed = meggaweb.settings.animation.speed;
    

    function init() {
      $itemBody.hide(); 
      addCloseButtons();
      loadImages();
      $trigger.on('click', toggleAccordion);
      setAccessibleAttrs();
      $itemWrapper.slice(0,2).find($trigger).trigger('click');
    }

    function toggleAccordion(e) {
      e.preventDefault();
        var $this = $(this),
            $wrap = $this.closest($itemWrapper),
            $content = $wrap.find($itemBody);
        if($wrap.hasClass('open')) {
            $wrap.removeClass('open');
            $content.slideUp(aniSpeed);
            $this.attr('title', meggaweb.constants.strings.common.seeMore).find('span').text(meggaweb.constants.strings.common.seeMore);
            meggaweb.utils.addAriaRole($content, 'aria-expanded', 'false');
        } else {
            $wrap.addClass('open');
            $content.slideDown(aniSpeed);
            $this.attr('title', meggaweb.constants.strings.common.seeLess).find('span').text(meggaweb.constants.strings.common.seeLess);
             meggaweb.utils.addAriaRole($content, 'aria-expanded', 'true');
        }
    }
    
    function addCloseButtons() {
        $itemWrapper.each(function(){
            var $this = $(this),
                $h2 = $('h2 a', $this),
                $accordionContent = $('div.content', $this),
                $closeBtn = $(meggaweb.constants.templates.common.paragaphLink);
                $('a', $closeBtn).text(meggaweb.constants.strings.common.close).parent().addClass('close-btn');
                meggaweb.utils.addAriaRole($('a', $closeBtn), 'aria-controls', $accordionContent.attr('id'));
                $accordionContent.append($closeBtn);
                $('a', $closeBtn).on('click', function(e){
                    e.preventDefault();
                    $h2.trigger('click');
                });    
        });
    }

    function setAccessibleAttrs() {

      $trigger.each(function(){
        var $this = $(this),
            val = $this.parents('article').hasClass('open') ? 'true' : 'false';
            meggaweb.utils.addAriaRole($this, 'aria-controls', $this.parents('article').find($itemBody).attr('id'));
            meggaweb.utils.addAriaRole($this.siblings('div.content'), 'aria-expanded', val);
      });
      
    }

   function loadImages() {
        $itemWrapper.each(function(){
            var $this = $(this), 
            self = $('img', $this), 
            filteredText;
            filteredText =  $('h2 a', $this).clone().children().remove().end().text();
            self.attr('src', self.data('img'));
           
           if(!meggaweb.settings.browser.ltIE9) {
            self.attr('alt', $.trim(filteredText));
          }
        });
    }

    return {
      init: init,
      setAccessibleAttrs: setAccessibleAttrs
    };

}();
var hijaxOverlay = function(){
	"use strict";
    var $trigger = $('a.hijax'),
        $header = $('header'),
        $overlay,
        $content,
        aniSpeed = meggaweb.settings.animation.speed;
    
    function init() {
		if (!$('body').hasClass('portfolio') || meggaweb.settings.browser.ltIE8) {
			return;
		}
		
        if (!meggaweb.utils.matchMedia(meggaweb.settings.MQ.smartphone)) {
			if ($('div.overlay', 'header').length === 0) {
				$overlay = $(meggaweb.constants.templates.hijax.overlay);
				$content = $('div.overlay-content', $overlay);
				$header.append($overlay);
			}
            bindEvents();
		} else {
            $('.container').off('click', 'a.hijax');
			if($('div.overlay').is(':visible')){
				$('div.overlay').hide();
			}
			setNav();
        }
      
    }
    
    function bindEvents() {
		
		$('li:first a', $header).on('click', closeOverlay);
		$content.on('click', 'p.close a', closeOverlay);
		$header.on('click', 'nav li a', setNav);
		
        $('.container').on('click', 'a.hijax', function(event){
            event.preventDefault();
            var $this = this;
            if ($overlay.is(':visible') && $this.href !== $content.data('active')){
                $content.slideUp(aniSpeed, function(){
                    $overlay.hide();
                    getHijaxContent($this.href);
                });
            } else if($overlay.is(':visible') && $this.href === $content.data('active')) {
				return;
			} else {
                getHijaxContent($this.href);   
            }
        });
    }
    
    function getHijaxContent(target) {
        var dataContent,
			$loader = $(meggaweb.constants.templates.common.ajaxLoader);
		if(!$trigger.hasClass('active')) {	
			$('li a', $header).each(function(){
				var $this = this;
				if ($this.href === target) {
					$('li a', $header).removeClass('active');
					$(this).addClass('active');	
				}
			});
		}
		
        $.ajax({
            type: 'GET',
            url: target
              })
			.always(function(){	
				$('li a.active', $header).append($loader);
				$overlay.attr('aria-busy', 'true');
			})
            .done(function(data) {
                dataContent = $('.ajax-content', $(data));
				$content.html(dataContent); 
            })
            .done(function() {
                var $closeBtn = $(meggaweb.constants.templates.hijax.closeButton);
				$closeBtn.find('a').text(meggaweb.constants.strings.hijax.closeButton);
				$('#hijax-content').append($closeBtn);
            })
            .fail(function(){
				removeLoader($loader);
				$overlay.attr('aria-busy', 'false');
				var str = $('<p></p>');
				str.html(meggaweb.constants.strings.hijax.error);
				$content.html(str);
                $overlay.fadeIn(aniSpeed, function(){
                    $content.slideDown(aniSpeed);
                });
		    })
            .complete(function(){
            	$overlay.fadeIn(aniSpeed, function(){
				   $content.slideDown(aniSpeed, function(){
						removeLoader($loader);
					});
                });
            	$content.data('active', target);
            	$overlay.attr('aria-busy', 'false').attr('tabindex', 0).focus();
			});
    }
	
	function setNav(e) {
		$('li a', $header).removeClass('active');
		if (e) {
			$(this).addClass('active');
		} else {
			$('li:first a', $header).addClass('active');
		}
	}
	
	function closeOverlay(event) {
		event.preventDefault();
			 if ($overlay.is(':visible')){
                $content.slideUp(aniSpeed, function(){
                    $overlay.hide();
					setNav();
					$overlay.attr('tabindex', -1);
					$('li a.active', $header).attr('tabindex', 0).focus();
                });
			}
	}
	
	function removeLoader(elem) {
		elem.fadeOut(aniSpeed, function(){
			elem.remove();
		});
	}
    
    return {
        init: init
    };

}();