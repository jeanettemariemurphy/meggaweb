if(typeof(meggaweb) === undefined || typeof(meggaweb) !== "object") {
  meggaweb = {};
}

meggaweb.constants = {
    
    templates: {
        carousel: {
          controls:  '<div class="controls" role="navigation"><a href="#" class="prev disabled" aria-disabled="true"><span class="visually-hidden">prev</span></a><a href="#" class="next"><span class="visually-hidden">next</span></a></div>',
          indicators: '<div class="carousel-indicators" role="navigation"><ul></ul></div>',
          triggers: '<li><a href="#"><span class="visually-hidden"></span></a></li>'
        },
        
        hijax: {
          overlay: '<div class="overlay" role="dialog" aria-labelledby="hijax-title" aria-describedby="hijax-content"><div class="overlay-inner"></div><div class="overlay-content"></div></div>',
          closeButton: '<p class="close"><a href="#"><span class="visually-hidden">Close overlay</span></a></p>'
        },
        
        common: {
          paragaphLink: '<p><a href="#"></a></p>',
          ajaxLoader: '<img class="loader-img" src="images/backgrounds/ajax-loader.gif" />'
        }
    },
    
    strings:  {
        hijax: {
          closeButton: 'Hide overlay',
          error: 'Sorry, there has been an error. Please try again later.'
        },
        common: {
          close: 'Close',
          seeLess: 'See Less',
          seeMore: 'See more'
        }
      }
}


 