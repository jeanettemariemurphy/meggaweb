
    var meggaweb = {
        
        settings: {
            animation: {
                speed: 500,
                easing: 'linear'
            },
            ajaxLoader: {
                speed: 500
            },
            browser: {
                IE: $('html').hasClass('ie'),
                ltIE7: $('html').hasClass('lt-ie7'),
                ltIE8: $('html').hasClass('lt-ie8'),
                ltIE9: $('html').hasClass('lt-ie9'),
                ltIE10: $('html').hasClass('lt-ie10')
            },
			MQ: {
				smartphone: 600
			}
        }
        
    }; // end function
	

    var alertFallback = false; // IE 6/7/8 console fall back - keep outside of doc ready
 
    if (typeof console === "undefined" || typeof console.log === "undefined") {
        console = {};
    if (alertFallback) {
        console.log = function(msg) {
         alert(msg);
        };
    } else {
     console.log = function() {};
         }
    }
