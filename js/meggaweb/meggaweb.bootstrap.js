if(typeof(meggaweb) === undefined || typeof(meggaweb) !== "object") {
  meggaweb = {};
}

meggaweb.bootstrap = {
	 init: function () {
	 	var self = this;
	 	self.resize(this);
		accordion.init();
		hijaxOverlay.init(); 
    },
	
	resize: function(self){
		$(window).resize(function() {
			hijaxOverlay.init();
		});
	}
};


$(function () {
	// init core js sitewide
	meggaweb.bootstrap.init();
    meggaweb.utils.init();
});