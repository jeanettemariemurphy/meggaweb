
var accordion = function () {
  var $wrapper = $('div.accordion'),
      $itemWrapper = $('article', $wrapper),
      $trigger = $('h2 a', $itemWrapper),
      $itemBody = $('div.content', $itemWrapper),
      aniSpeed = meggaweb.settings.animation.speed;
    

    function init() {
      $itemBody.hide(); 
      addCloseButtons();
      loadImages();
      $trigger.on('click', toggleAccordion);
      setAccessibleAttrs();
      $itemWrapper.slice(0,2).find($trigger).trigger('click');
    }

    function toggleAccordion(e) {
      e.preventDefault();
        var $this = $(this),
            $wrap = $this.closest($itemWrapper),
            $content = $wrap.find($itemBody);
        if($wrap.hasClass('open')) {
            $wrap.removeClass('open');
            $content.slideUp(aniSpeed);
            $this.attr('title', meggaweb.constants.strings.common.seeMore).find('span').text(meggaweb.constants.strings.common.seeMore);
            meggaweb.utils.addAriaRole($content, 'aria-expanded', 'false');
        } else {
            $wrap.addClass('open');
            $content.slideDown(aniSpeed);
            $this.attr('title', meggaweb.constants.strings.common.seeLess).find('span').text(meggaweb.constants.strings.common.seeLess);
             meggaweb.utils.addAriaRole($content, 'aria-expanded', 'true');
        }
    }
    
    function addCloseButtons() {
        $itemWrapper.each(function(){
            var $this = $(this),
                $h2 = $('h2 a', $this),
                $accordionContent = $('div.content', $this),
                $closeBtn = $(meggaweb.constants.templates.common.paragaphLink);
                $('a', $closeBtn).text(meggaweb.constants.strings.common.close).parent().addClass('close-btn');
                meggaweb.utils.addAriaRole($('a', $closeBtn), 'aria-controls', $accordionContent.attr('id'));
                $accordionContent.append($closeBtn);
                $('a', $closeBtn).on('click', function(e){
                    e.preventDefault();
                    $h2.trigger('click');
                });    
        });
    }

    function setAccessibleAttrs() {

      $trigger.each(function(){
        var $this = $(this),
            val = $this.parents('article').hasClass('open') ? 'true' : 'false';
            meggaweb.utils.addAriaRole($this, 'aria-controls', $this.parents('article').find($itemBody).attr('id'));
            meggaweb.utils.addAriaRole($this.siblings('div.content'), 'aria-expanded', val);
      });
      
    }

   function loadImages() {
        $itemWrapper.each(function(){
            var $this = $(this), 
            self = $('img', $this), 
            filteredText;
            filteredText =  $('h2 a', $this).clone().children().remove().end().text();
            self.attr('src', self.data('img'));
           
           if(!meggaweb.settings.browser.ltIE9) {
            self.attr('alt', $.trim(filteredText));
          }
        });
    }

    return {
      init: init,
      setAccessibleAttrs: setAccessibleAttrs
    };

}();