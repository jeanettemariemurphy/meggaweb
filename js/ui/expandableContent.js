var expandableContent = function () {
 var $triggerContainer = $('.toggle-container'),
       $expandableContent = $('.expandable-content', $triggerContainer), 
        aniSpeed = meggaweb.settings.animation.speed;
		
        $expandableContent.each(function(){
           var $this = $(this);
				$this.data('height', $this.height());
		
           $this.animate({
            height: 0
           }, aniSpeed)
           $this.hide();
        });
      
		$triggerContainer.each(function(){
			
			var $self = $(this),
				$triggerOpen = $('.show-trigger', $self),
				$triggerClose = $('.close-trigger', $self);
		
			$triggerOpen.on('click', function(e){
				e.preventDefault();
				var $this = $(this),
					$content = $this.closest($triggerContainer).find($expandableContent),
				    hgt = $content.data('height');
					
				toggleContent($content, hgt);
				$this.closest($triggerOpen).addClass('visibility-hidden');
		   })
			
			
			$triggerClose.on('click', function(e){
			    e.preventDefault();
				
			    var $this = $(this),
					$content = $this.closest($expandableContent);
				   hgt = $content.data('height');
				   toggleContent($content, 0);
				   
			})
		  
		})
		
		
         function toggleContent(elem, hgt){
               elem.show().animate({
                height: hgt
            }, aniSpeed, function(){
				if(hgt === 0){
					$(this).closest($triggerContainer).find($('.show-trigger')).removeClass('visibility-hidden');
                     $(this).hide();
					
					meggaweb.utils.addAriaRole(elem, 'aria-hidden', 'true');
					
				} else {        
                 
					meggaweb.utils.addAriaRole(elem, 'aria-hidden', 'false');
				}
            })
               
         }
		 
		 return {
			toggleContent: toggleContent
		 }
   


}();