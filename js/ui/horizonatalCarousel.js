var slideShowCarousel = function () {
  "use strict";
    var $container,
        $panel,
        $controls,
        $indicators,
        $triggers,
        panelWidth,
        panelLength,
        activePanel = null,
        firstLoad = true,
        aniSpeed = meggaweb.settings.animation.speed;
          
        function init() {
            if (firstLoad === true) {
            $container = $('.slideshow-carousel');
            $panel = $('article', $container);
            activePanel = activePanel !== null? activePanel : 0;
            panelWidth = $panel.width();
            panelLength = $panel.length;
            setViewportHeight();
            loadImages();
            bindEvents();
            setIndicators();
            createControls();
            $panel.eq(activePanel).css('left', 0);
            firstLoad = false;
            } else {
                 setViewportHeight();
            }
        }
			
        function setViewportHeight(){
            $container.css('minHeight', $panel.eq(activePanel).height());
        }
          
		function bindEvents(){
            $container.on('mouseenter', showControls);
            $container.on('mouseleave', hideControls);
            $container.on('focus', tabCarousel);
        }
        
        function createControls() {
            $controls = $(meggaweb.constants.templates.carousel.controls);
            $container.append($controls);
            $controls.on('click', 'a', controlsReset);
        }
        
         function setIndicators() {
            var trigs;
            $indicators = $(meggaweb.constants.templates.carousel.indicators);
            for (var i = 0; i < panelLength; i++) {
                trigs = $(meggaweb.constants.templates.carousel.triggers);
                var txt = $panel.eq(i).find('h2').text();
                $('a span', trigs).text(txt).attr('title', txt);
                $('ul', $indicators).append(trigs);
            }
                
            $container.after($indicators);
            $triggers = $('li', $indicators);
            $('a', $triggers.eq(0)).addClass('disabled');
            meggaweb.utils.addAriaRole($('a', $triggers.eq(0)), 'aria-disabled', 'true');
            $indicators.on('click', 'a', indicatorReset);
        }
        
        function showControls() {
            if (!$container.hasClass('accordion')) {
                 $controls.fadeIn(aniSpeed);
            } 
        }
        
        function hideControls() {
            $controls.fadeOut(aniSpeed);
        }

        function tabCarousel(e) {
            var negTabs = $('article', $container);
            meggaweb.utils.removeElemsFromTabFlow(negTabs);
            $container.on('keydown', onKeyDown);
        }

        function onKeyDown(e){
            var keyCode = e.keyCode || e.which; 
            
            e.stopPropagation();

            switch(keyCode) {
                case 37: // left arrow
                case 38:  // up arrow

                keyboardReset();
                break;

                case 39: // right
                case 40:  // down

                keyboardReset('next');
                break;

                case 27: // escape

                $container.focus();
                meggaweb.utils.removeElemsFromTabFlow($panel);
                break;

            }
           
        }
        
        function indicatorReset(event) {
            event.preventDefault();
            tabCarousel();
            var $this = $(this),
                index = $(this).parent().index();
            if ((!$panel.is(':animated')) && (!$this.hasClass('disabled'))) {
                resetCarousel(index);
            }  
        }
        
         function controlsReset(event) {

            event.preventDefault();
            tabCarousel();
            var $this = $(this), 
                index = activePanel;
            
            if (!$panel.is(':animated')) {
                if ($this.hasClass('next') && activePanel < panelLength - 1) {
                    index++;
                    resetCarousel(index);
                } 
                
                else if($this.hasClass('prev') && activePanel > 0) {
                    index--;
                    resetCarousel(index);
                } 
            }  
        }

        function keyboardReset(nxt) {

            var index = activePanel;
            if (!$panel.is(':animated')) {
                if (nxt && activePanel < panelLength - 1) {
                    console.log('forward');
                    index++;
                    resetCarousel(index);
                } 
                
                else if(!nxt && activePanel > 0) {
                    console.log('back');
                    index--;
                    resetCarousel(index);
                } 
            }  
        }
        
        function resetCarousel(index) {
            var direction;
            if (index < activePanel) {
                $panel.eq(index).addClass('prev');
                direction = "1000px";
            }  else {
                direction = "-1000px";
            }
            
            $panel.eq(activePanel).fadeOut(200, function(){
                $(this).attr('style', "");
                meggaweb.utils.removeElemsFromTabFlow($(this));
            });
            activePanel = index;
            setActivePanel($panel.eq(activePanel)); 
        }
			 
        function setActivePanel(panel){
            
			var hgt = panel.height();
			$($container).animate({
			   minHeight: hgt
			}, aniSpeed);
			   
            $(panel, $container).delay(400).animate({
                left: 0
            }, aniSpeed, function() {
                var $this = $(this);
                if ($this.hasClass('prev')) {
                    $this.removeClass('prev');
                }
                meggaweb.utils.focusElement($this);
                setDisabledControls();
            });
		}
        
        function setDisabledControls() {
            $('a', $controls).removeClass('disabled');
            meggaweb.utils.addAriaRole($('a', $controls), 'aria-disabled', 'false');
            
            if (activePanel === panelLength - 1) {
               $('a.next', $controls).addClass('disabled');
               meggaweb.utils.addAriaRole($('a.next', $controls), 'aria-disabled', 'true');
            } else if (activePanel === 0) {
                $('a.prev', $controls).addClass('disabled');
                meggaweb.utils.addAriaRole($('a.prev', $controls), 'aria-disabled', 'true');
            } 
            
            $triggers.each(function(){
                    var $self = $(this);
                   if($self.find('a.disabled')) {
                    $('a', $self).removeClass('disabled');
                    meggaweb.utils.addAriaRole($('a', $self), 'aria-disabled', 'false');
                   }
                });
                $('a',$triggers).eq(activePanel).addClass('disabled');
                meggaweb.utils.addAriaRole($('a',$triggers).eq(activePanel), 'aria-disabled', 'true');
        }
        
        function loadImages() {
            $panel.each(function(){
                var $this = $(this), self = $('img', $this);
                self.attr('src', self.data('img'));
                self.attr('title', $('h2', $this).text());
            });
        }
        
        return {
            init: init
        };

}();