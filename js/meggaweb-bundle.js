
    var meggaweb = {
        
        settings: {
            animation: {
                speed: 500,
                easing: 'linear'
            },
            ajaxLoader: {
                speed: 500
            },
            browser: {
                IE: $('html').hasClass('ie'),
                ltIE7: $('html').hasClass('lt-ie7'),
                ltIE8: $('html').hasClass('lt-ie8'),
                ltIE9: $('html').hasClass('lt-ie9'),
                ltIE10: $('html').hasClass('lt-ie10')
            },
			MQ: {
				smartphone: 600
			}
        }
        
    }; // end function
	

    var alertFallback = false; // IE 6/7/8 console fall back - keep outside of doc ready
 
    if (typeof console === "undefined" || typeof console.log === "undefined") {
        console = {};
    if (alertFallback) {
        console.log = function(msg) {
         alert(msg);
        };
    } else {
     console.log = function() {};
         }
    }

﻿if(typeof(meggaweb) === undefined || typeof(meggaweb) !== "object") {
  meggaweb = {};
}

meggaweb.utils = {
    
    init: function () {
	
		var self = this;
        self.bindEvents();
	
	},
	
	bindEvents: function(){	
	},
        
   	addAriaRole: function (elem, ariaRole, roleVal) {
		 var obj =  $(elem); //element to apply the role to
		 if (obj) {
			// Add the aria role property and value to the element
			$(obj).attr(ariaRole, roleVal);
		}
	},
   
	addOverlay: function(elem){
	var overlay = '<div class="loading"></div>',
          hgt = elem.height(),
          wdh = elem.width();
          
       $(overlay).css({
          width: wdh,
          height: hgt
        })
       .fadeIn()
       .appendTo(elem);
	
	},
	
	removeOverlay: function(elem){
		 $('.loading',elem)
        .fadeOut()
        .remove();    
	},

	smoothScroll: function (elem, offsetMargin) {
		offsetMargin = offsetMargin? offsetMargin : 10;
		 var offset = elem.offset(),
			offsetFinal = offset.top - offsetMargin;

		  $('body, html').stop().animate({
			  scrollTop: offsetFinal
		  },500);
    },	
  
	  matchMedia: function(media){
		return parseInt($(document).width()) < media;
	  },

	  removeElemsFromTabFlow: function(elems) {
	  	for (var i = 0;i<elems.length;i++) {
	  		var $el = $(elems[i]);
	  		$el.attr('tabindex', -1).find('a').attr('tabindex', -1);
	  	};
	  },

	  focusElement: function(el) {
	  	el.attr('tabindex', 0).focus().find('a').attr('tabindex', 0);
	  },

	  addAttr: function(el, attr, val) {
	  	var $el = $(el);
	  	for (var i = 0; i < el.length; i++){
	  		
	  		$el.attr(attr, val);
	  	}
	  	
	  },

	  removeAttrs: function(el, attr){
	  	var $el = $(el);
	  	for (var i = 0; i < el.length; i++){
	  		
	  		for(var g = 0; g<attr.length; g++) {
	  			$el.removeAttr(attr[g]);
	  		}
	  		
	  	}
	  	
	  }
};
      


if(typeof(meggaweb) === undefined || typeof(meggaweb) !== "object") {
  meggaweb = {};
}

meggaweb.constants = {
    
    templates: {
        carousel: {
          controls:  '<div class="controls" role="navigation"><a href="#" class="prev disabled" aria-disabled="true"><span class="visually-hidden">prev</span></a><a href="#" class="next"><span class="visually-hidden">next</span></a></div>',
          indicators: '<div class="carousel-indicators" role="navigation"><ul></ul></div>',
          triggers: '<li><a href="#"><span class="visually-hidden"></span></a></li>'
        },
        
        hijax: {
          overlay: '<div class="overlay" role="dialog" aria-labelledby="hijax-title" aria-describedby="hijax-content"><div class="overlay-inner"></div><div class="overlay-content"></div></div>',
          closeButton: '<p class="close"><a href="#"><span class="visually-hidden">Close overlay</span></a></p>'
        },
        
        common: {
          paragaphLink: '<p><a href="#"></a></p>',
          ajaxLoader: '<img class="loader-img" src="images/backgrounds/ajax-loader.gif" />'
        }
    },
    
    strings:  {
        hijax: {
          closeButton: 'Hide overlay',
          error: 'Sorry, there has been an error. Please try again later.'
        },
        common: {
          close: 'Close',
          seeLess: 'See Less',
          seeMore: 'See more'
        }
      }
}


 
if(typeof(meggaweb) === undefined || typeof(meggaweb) !== "object") {
  meggaweb = {};
}

meggaweb.bootstrap = {
	 init: function () {
	 	var self = this;
	 	self.resize(this);
		accordion.init();
		hijaxOverlay.init(); 
    },
	
	resize: function(self){
		$(window).resize(function() {
			hijaxOverlay.init();
		});
	}
};


$(function () {
	// init core js sitewide
	meggaweb.bootstrap.init();
    meggaweb.utils.init();
});